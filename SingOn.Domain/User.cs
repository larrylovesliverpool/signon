﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignOn.Domain
{
    public class User
    {
        public string employeeNumber { get; set; }
        public string userName { get; set; }
        public string name { get; set; }
        public string password { get; set; }
        public bool active { get; set; }
    }
}
