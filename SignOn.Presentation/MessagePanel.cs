﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignOn.Presentation
{
    public class MessagePanel : IMessagePanel
    {
        public string Message { get; set; }

        public string StatusCode { get; set; }

        public string DisplayColour { get; set; }

        public string DisplayIcon { get; set; }

        public bool Visible { get; set; }
    }
}
