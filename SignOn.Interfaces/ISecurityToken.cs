﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignOn.Interfaces
{
    public interface ISecurityToken
    {
        string CreateToken(string userName);

        string GetSecurityToken();

        DateTime GetWhen(string token);

        Guid GetKey(string token);

        string GetuserName(string token);

        bool IsValid(string token);
    }
}
