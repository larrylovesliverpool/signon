﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignOn.Interfaces
{
    public interface IPageSecurity
    {
        bool IsPageValid();
    }
}
