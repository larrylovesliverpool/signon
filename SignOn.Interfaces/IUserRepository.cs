﻿using System.Collections.Generic;
using SignOn.Domain;

namespace SignOn.Interfaces
{
    public interface IUserRepository
    {
        List<User> GetUsersDetails();

        User GetUserDetails(string userName);

        User GetUserDetails(int userID);
    }
}
