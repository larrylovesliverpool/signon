﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignOn.ViewModel
{
    public class HomePageVM : IHomePageVM
    {
        public string UserName;
        
        public HomePageVM Generate()
        {
            throw new NotImplementedException();
        }

        public HomePageVM Generate(string SecurityID)
        {
            var homePage = new Orchestration.HomePage(SecurityID).GeneratePage();

            return this;
        }
    }
}