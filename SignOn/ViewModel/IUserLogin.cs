﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignOn.ViewModel
{
    public interface IUserLogin
    {
        UserLoginVM GenerateViewModel();
    }
}
