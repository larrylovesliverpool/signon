﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignOn.ViewModel
{
    public interface IHomePageVM
    {
        HomePageVM Generate();

        HomePageVM Generate(string SecurityID);
    }
}
