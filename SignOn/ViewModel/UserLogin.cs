﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SignOn.ViewModel
{
    public class UserLoginVM : BaseModel.BaseModelVM, IUserLogin
    {
        [Required]
        [Display(Name = "User Name")]
        [StringLength(25)]
        public string userName { get; set; }

        [Required]
        [Display(Name = "Password")]
        [StringLength(25)]
        public string password { get; set; }

        public UserLoginVM GenerateViewModel() => this;
    }
}