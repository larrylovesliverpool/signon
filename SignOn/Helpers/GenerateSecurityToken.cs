﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignOn.Helpers
{
    public class GenerateSecurityToken : Interfaces.ISecurityToken
    {
        private string _securityToken;

        public string SecurityToken
        {
            get
            {
                return _securityToken;
            }

            set
            {
                _securityToken = value;
            }
        }

        public GenerateSecurityToken() { }

        public GenerateSecurityToken(string userName)
        {
            SecurityToken = CreateToken(userName);
        }

        public string CreateToken(string userName)
        {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            byte[] user = Encoding.ASCII.GetBytes(userName);

            string token = Convert.ToBase64String(time.Concat(key).Concat(user).ToArray());

            return token;
        }

        public string GetSecurityToken()
        {
            return SecurityToken;
        }

        public DateTime GetWhen(string token)
        {
            byte[] data = Convert.FromBase64String(token);
            DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));

            return when;
        }

        public bool IsValid(string token)
        {
            throw new NotImplementedException();
        }

        public Guid GetKey(string token)
        {
            byte[] data = Convert.FromBase64String(token);
            var guid = data.Skip(8).Take(16).ToArray();

            Guid key = new Guid(guid);

            return key;
        }

        public string GetuserName(string token)
        {
            byte[] data = Convert.FromBase64String(token);
            var userName = data.Skip(24).Take(data.Count()-24).ToArray();

            return Encoding.ASCII.GetString(userName);
        }

        public string GetSecurityToken(string token)
        {
            throw new NotImplementedException();
        }
    }
}