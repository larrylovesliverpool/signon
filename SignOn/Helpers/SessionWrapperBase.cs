﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignOn.Helpers
{
    public class SessionWrapperBase
    {
        public T GetSession<T>(string index)
        {
            return (T)HttpContext.Current.Session[index];
        }

        public void SetSession<T>(string index, T Value)
        {
            HttpContext.Current.Session[index] = Value;
        }

        public void RemoveSession(string index)
        {
            HttpContext.Current.Session.Remove(index);
        }

        public bool IsValid(string index)
        {
            if (HttpContext.Current.Session[index] != null)
                return true;

            return false;
        }
    }
}