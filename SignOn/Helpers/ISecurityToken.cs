﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignOn.Helpers
{
    public interface ISecurityToken
    {
        bool HasToken();
    }
}
