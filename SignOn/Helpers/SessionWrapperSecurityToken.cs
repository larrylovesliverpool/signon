﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SignOn.Models;

namespace SignOn.Helpers
{
    public class SessionWrapperSecurityToken : SessionWrapperBase, ISecurityToken
    {
        public string Read() => GetSession<string>("SecurityToken");

        public void Create(string securityToken) => SetSession<string>("SecurityToken", securityToken);

        public void Destory() => RemoveSession("SecurityToken");

        public bool HasToken()
        {
            throw new NotImplementedException();
        }
    }

}