﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SignOn.Helpers;
using SignOn.ViewModel;
using SignOn.Orchestration;

namespace SignOn.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet]
        public ActionResult Login() => View(new UserLoginVM());

        [HttpPost]
        public ActionResult Login(UserLoginVM loginVM)
        {

            try
            {
                if( ModelState.IsValid)
                {
                    Login userLogin = new Login(loginVM.userName.Trim(), loginVM.password.Trim()); 

                    if( userLogin.IsLoggedIn)
                    {
                        Interfaces.ISecurityToken token = new SignOn.Helpers.GenerateSecurityToken(loginVM.userName.Trim());

                        SessionWrapperSecurityToken SecurityToken = new SessionWrapperSecurityToken();

                        SecurityToken.Create(token.GetSecurityToken());
                    }
                }
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

    }
}