﻿using SignOn.Helpers;
using SignOn.Helpers.Filter;
using System;
using System.Collections.Specialized;
using System.Web.Mvc;

namespace SignOn.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [HttpHeader]
        public ActionResult Index()
        {
            SessionWrapperSecurityToken SecuritySession = new SessionWrapperSecurityToken();

            return View();
        }
    }
}