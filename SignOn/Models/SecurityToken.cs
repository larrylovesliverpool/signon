﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignOn.Models
{
    public class SecurityToken
    {
        private string _userIdentifier;

        public SecurityToken() { }

        public SecurityToken(string UserIdentifier)
        {
            this._userIdentifier = UserIdentifier;
        }

        public string UserIdentifier
        {
            get
            {
                return _userIdentifier;
            }
        }
    }
}