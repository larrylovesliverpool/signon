﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignOn.Orchestration.BaseModel
{
    public class SecurityToken
    {
        public string token { get; }

        public SecurityToken() { }

        public SecurityToken(string token) {

            this.token = token;
        }
    }
}
