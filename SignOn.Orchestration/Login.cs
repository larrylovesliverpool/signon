﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignOn.Orchestration
{
    public class Login
    {
        Interfaces.IUserRepository userRepo;

        public bool IsLoggedIn { get; set; }

        public int userID { get; set; }

        public Login()
        {
            userRepo = new Repository.User();
            IsLoggedIn = false;
        }

        public Login(string userName, string password) : this()
        {
            validateUser(userName, password);
        }

        private void validateUser(string userName, string password)
        {
            //IsLoggedIn = userRepo.GetUsersDetails().Exists(x => x.userName == userName && x.password == password);

            IsLoggedIn = true;
        }
    }
}
